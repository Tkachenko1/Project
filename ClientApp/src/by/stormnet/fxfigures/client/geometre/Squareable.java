package by.stormnet.fxfigures.client.geometre;

public interface Squareable {
    double getSquare();
    default double getDouble(){
        return 123.123;
    }
}


package by.stormnet.fxfigures.client.controller;

import by.stormnet.fxfigures.client.ClientApp;
import by.stormnet.fxfigures.client.geometre.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.Random;
import java.util.ResourceBundle;

public class MainViewController implements Initializable {
    private Figure[] figures;
    private Random rnd;


    @FXML
    private Canvas canvas;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        figures = new Figure[1];
        rnd = new Random(System.currentTimeMillis());
    }

    @FXML
    private void onMouseClicked(MouseEvent mouseEvent) {
        //System.out.println("x: " + mouseEvent.getX());
        //System.out.println("y: " + mouseEvent.getY());
        addFigure(createFigure(mouseEvent.getX(), mouseEvent.getY()));
        repaint();
    }




    private Figure createFigure(double x, double y) {
        Figure figure = null;
        double lineWidth = rnd.nextInt(6);
        double r = rnd.nextDouble();
        double g = rnd.nextDouble();
        double b = rnd.nextDouble();
        Color randomColor = new Color(r, g, b, 1);
        switch (rnd.nextInt(3)) {
            case Figure.FIGURE_TYPE_CIRCLE:
                double radius = rnd.nextInt(101);
                //System.out.println("Круг {координаты: X=радиус=" +radius+", ");
                figure = new Circle(x, y, lineWidth == 0 ? 1 : lineWidth, randomColor,
                        radius < 10 ? 10 : radius);
                break;
            case Figure.FIGURE_TYPE_RECTANGLE:
                //System.out.println("Прямоугольник");
                double width = rnd.nextInt(101);
                double height = rnd.nextInt(101);
                figure = new Rectangle(x, y, lineWidth == 0 ? 1 : lineWidth, randomColor,
                        width < 10 ? 10 : width, height < 10 ? 10 : height);

                break;
            case Figure.FIGURE_TYPE_TRIANGLE:
                //System.out.println("Треугольник");
                double base = rnd.nextInt(101);
                figure = new Triangle(x, y, lineWidth == 0 ? 1 : lineWidth, randomColor,
                        base < 10 ? 10 : base);
                break;
            default:
                System.out.println("unknown figure type.");
        }

        return figure;
    }

    private void addFigure(Figure figure) {
        if (figure == null)
            figures = new Figure[1];
        if (figures[figures.length - 1] == null) {
            figures[figures.length - 1] = figure;
            return;
        }

        Figure[] tmp = new Figure[figures.length + 1];
        int index = 0;
        for (; index < figures.length; index++) {
            tmp[index] = figures[index];
        }
        tmp[index] = figure;
        figures = tmp;

    }

    private void repaint() {
        if (figures == null)
            return;
        canvas.getGraphicsContext2D().clearRect(0, 0, ClientApp.SCREEN_WIDTH, ClientApp.SCREEN_HEIGHT);
        for (Figure figure : figures) {
            if (figure != null) {
                figure.draw(canvas.getGraphicsContext2D());
            }

        }
    }
    @FXML
    public void onAction(ActionEvent actionEvent) {
        printAllFiguresInfo(figures);
        printOveralSquare(figures);
    }

    private void printAllFiguresInfo(Printable[] printables){
        for (Printable printable : printables) {
            printable.printInfo();
        }
        System.out.println("=============================");
    }

    private void printOveralSquare(Squareable[] squareables){
        double OveralSquare = 0.0;
        for (Squareable squareable: squareables){
            OveralSquare = OveralSquare + squareable.getSquare() ;
        }
        System.out.println("Общая площадь всех фигур равна - " + OveralSquare);
    }
}






package by.stormnet.fxfigures.client;

import by.stormnet.fxfigures.client.geometre.Printable;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

public class ClientApp extends Application {
    public static final int SCREEN_WIDTH = 1100;
    public static final int SCREEN_HEIGHT = 700;

    public static void main(String[] args) {
        launch();
    }


    @Override
    public void start(Stage wnd) throws Exception {
        wnd.setTitle("MegaFxFigure");
        Parent root = FXMLLoader.load(getClass().getResource("/views/MainView.fxml"));
        wnd.setScene(new Scene(root, SCREEN_WIDTH, SCREEN_HEIGHT));
        wnd.show();
    }


}

package by.stormnet.fxfigures.client.geometre;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

public class Circle extends Figure {

    private double radius;


    public Circle(double cX, double cY, double lineWidth, Color color) {
        super(FIGURE_TYPE_CIRCLE, cX, cY, lineWidth, color);
    }

    public Circle( double cX, double cY, double lineWidth, Color color, double radius) {
        this( cX, cY, lineWidth, color);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setFill(color);
        gc.setLineWidth(LineWidth);
        gc.fillOval(cX-radius/2,cY-radius/2,radius,radius);
    }
    //double square = Math.PI *(radius*radius);

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Круг {");
        sb.append("координата X=").append(cX);
        sb.append(", кордината Y=").append(cY);
        sb.append(", радиус=").append(radius);
        sb.append(", толщина отрисовки линии круга=").append(LineWidth);
        sb.append(", цвет=").append(color);
        sb.append(", площадь круга=").append(Math.PI *(radius*radius));
        sb.append('}');
        return sb.toString();
    }

    @Override
    public void printInfo () { System.out.println(toString());}

    @Override
    public double getSquare() {
        double square = Math.PI *(radius*radius);
        return square;
    }

}
